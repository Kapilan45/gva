<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\Index;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\VolRepository")
 */
class Vol
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $depart;

    /**
     * @ORM\Column(type="string")
     */
    private $arrivee;

    /**
     * @ORM\Column(type="datetime")
     */
    private $heure_depart;

    /**
     * @ORM\Column(type="datetime")
     */
    private $heure_arrivee;

    /**
     * @ORM\Column(type="time")
     */
    private $duree;


    /**
     * @ORM\Column(type="integer")
     */
    private $numero_vol;

    /**
     * @ORM\Column(type="integer")
     */
    private $prix;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Aeroports" )
     * @ORM\JoinColumn(name="depart", referencedColumnName="ville")
    */
    private $depart_aeroports;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Aeroports")
     * @ORM\JoinColumn(name="arrivee", referencedColumnName="ville")
     */
    private $arrivee_aeroports;

    /**
     * @ORM\Column(type="string")
     */
    private $compagnies;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Compagnies")
     * @ORM\JoinColumn(name="compagnies", referencedColumnName="nom")
     */
    private $vol_compagnies;

    /**
     * @ORM\Column(type="integer")
     */
    private $places = 50;


    public function getId(): ?int
    {
        return $this->id;
    }


    public function getDepart(): ?string
    {
        return $this->depart;
    }

    public function setDepart(string $depart): self
    {
        $this->depart = $depart;

        return $this;
    }

    public function getArrivee(): ?string
    {
        return $this->arrivee;
    }

    public function setArrivee(string $arrivee): self
    {
        $this->arrivee = $arrivee;

        return $this;
    }
    

    public function getHeureDepart(): ?\DateTimeInterface
    {
        return $this->heure_depart;
    }

    public function setHeureDepart(\DateTimeInterface $heure_depart): self
    {
        $this->heure_depart = $heure_depart;

        return $this;
    }

    public function getHeureArrivee(): ?\DateTimeInterface
    {
        return $this->heure_arrivee;
    }

    public function setHeureArrivee(\DateTimeInterface $heure_arrivee): self
    {
        $this->heure_arrivee = $heure_arrivee;

        return $this;
    }

    public function getDuree(): ?\DateTimeInterface
    {
        return $this->duree;
    }

    public function setDuree(\DateTimeInterface $duree): self
    {
        $this->duree = $duree;

        return $this;
    }


    public function getNumeroVol(): ?int
    {
        return $this->numero_vol;
    }

    public function setNumeroVol(int $numero_vol): self
    {
        $this->numero_vol = $numero_vol;

        return $this;
    }


    public function getPrix(): ?int
    {
        return $this->prix;
    }

    public function setPrix(int $prix): self
    {
        $this->prix = $prix;

        return $this;
    }


    public function getDepartAeroports(): ?Aeroports
    {
        return $this->depart_aeroports;
    }

    public function setDepartAeroports(?Aeroports $depart_aeroports): self
    {
        $this->depart_aeroports = $depart_aeroports;

        return $this;
    }

    public function getArriveeAeroports(): ?Aeroports
    {
        return $this->arrivee_aeroports;
    }

    public function setArriveeAeroports(?Aeroports $arrivee_aeroports): self
    {
        $this->arrivee_aeroports = $arrivee_aeroports;

        return $this;
    }

    public function getCompagnies(): ?string
    {
        return $this->compagnies;
    }

    public function setCompagnies(string $compagnies): self
    {
        $this->compagnies = $compagnies;

        return $this;
    }

    public function getVolCompagnies(): ?Compagnies
    {
        return $this->vol_compagnies;
    }

    public function setVolCompagnies(Compagnies $vol_compagnies): self
    {
        $this->vol_compagnies = $vol_compagnies;

        return $this;
    }

    public function getPlaces(): ?int
    {
        return $this->places;
    }

    public function setPlaces(int $places): self
    {
        $this->places = $places;

        return $this;
    }

    public function getDateDeparts(): ?Aeroports
    {
        return $this->dateDeparts;
    }

    public function setDateDeparts(?Aeroports $dateDeparts): self
    {
        $this->dateDeparts = $dateDeparts;

        return $this;
    }


    
}
