<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BilletRepository")
 */
class Billet
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $num_billet;

    /**
     * @ORM\Column(type="integer")
     */
    private $client_id;

    /**
     * @ORM\Column(type="integer")
     */
    private $vol_id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Client", inversedBy="billet")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     */
    private $client;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Vol")
     * @ORM\JoinColumn(name="vol_id", referencedColumnName="id")
     */
    private $vol;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $prix;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $escale_id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Escale")
     * @ORM\JoinColumn(name="escale_id", referencedColumnName="id")
     */
    private $escale;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $billet;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $checkin;

    /**
     * @ORM\Column(type="integer")
     */
    private $place = 0;


    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function getNumBillet(): ?int
    {
        return $this->num_billet;
    }

    public function setNumBillet(int $num_billet): self
    {
        $this->num_billet = $num_billet;

        return $this;
    }

    public function getClientId(): ?int
    {
        return $this->client_id;
    }

    public function setClientId(int $client_id): self
    {
        $this->client_id = $client_id;

        return $this;
    }

    public function getVolId(): ?int
    {
        return $this->vol_id;
    }

    public function setVolId(int $vol_id): self
    {
        $this->vol_id = $vol_id;

        return $this;
    }

    public function getClient(): ?Client
    {
        return $this->client;
    }

    public function setClient(?Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getVol(): ?Vol
    {
        return $this->vol;
    }

    public function setVol(?Vol $vol): self
    {
        $this->vol = $vol;

        return $this;
    }

    public function getPrix(): ?float
    {
        return $this->prix;
    }

    public function setPrix(?float $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    public function getEscaleId(): ?int
    {
        return $this->escale_id;
    }

    public function setEscaleId(?int $escale_id): self
    {
        $this->escale_id = $escale_id;

        return $this;
    }

    public function getEscale(): ?Escale
    {
        return $this->escale;
    }

    public function setEscale(?Escale $escale): self
    {
        $this->escale = $escale;

        return $this;
    }

    public function getBillet(): ?bool
    {
        return $this->billet;
    }

    public function setBillet(?bool $billet): self
    {
        $this->billet = $billet;

        return $this;
    }

    public function getCheckin(): ?bool
    {
        return $this->checkin;
    }

    public function setCheckin(?bool $checkin): self
    {
        $this->checkin = $checkin;

        return $this;
    }

    public function getPlace(): ?int
    {
        return $this->place;
    }

    public function setPlace(int $place): self
    {
        $this->place = $place;

        return $this;
    }



}
