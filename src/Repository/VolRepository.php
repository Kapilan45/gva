<?php

namespace App\Repository;

use App\Entity\Vol;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Vol|null find($id, $lockMode = null, $lockVersion = null)
 * @method Vol|null findOneBy(array $criteria, array $orderBy = null)
 * @method Vol[]    findAll()
 * @method Vol[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VolRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Vol::class);
    }

    /**
     * @return Vol[] Returns an array of Vol objects
     *
    */

    public function findByDateVol($date, $depart, $arrivee)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.heure_depart >= :date AND v.depart = :depart AND v.arrivee = :arrivee')
            ->setParameter('date', $date)
            ->setParameter('depart', $depart)
            ->setParameter('arrivee', $arrivee)
            ->orderBy('v.heure_depart', 'ASC')
            ->setMaxResults(20)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Vol[] Returns an array of Vol objects
     *
    */

    public function findByCarte($today, $date, $id)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.id = :id AND v.heure_depart > :date AND v.heure_depart LIKE :today')
            ->setParameter('date', $date)
            ->setParameter('today', $today.'%')
            ->setParameter('id', $id)
            ->setMaxResults(1)
            ->getQuery()
            ->getResult()
        ;
    }


    /**
     * @return Vol[] Returns an array of Vol objects
     *
    */

    public function findByEscaleVol($dateD, $dateR, $np)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.heure_depart >= :dateD AND v.heure_arrivee <= :dateR')
            ->andWhere('v.places >= :places')
            ->setParameter('dateD', $dateD)
            ->setParameter('dateR', $dateR)
            ->setParameter('places', $np)
            ->orderBy('v.heure_depart', 'ASC')
            ->setMaxResults(20)
            ->getQuery()
            ->getResult()
        ;
    }


    public function findByVolDirect($dateD, $depart, $arrivee, $np)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.heure_depart >= :dateD AND v.depart = :depart AND v.arrivee = :arrivee')
            ->andWhere('v.places >= :places')
            ->setParameter('dateD', $dateD)
            ->setParameter('depart', $depart)
            ->setParameter('arrivee', $arrivee)
            ->setParameter('places', $np)
            ->orderBy('v.heure_depart', 'ASC')
            ->setMaxResults(20)
            ->getQuery()
            ->getResult()
        ;
    }


    // /**
    //  * @return Vol[] Returns an array of Vol objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Vol
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
