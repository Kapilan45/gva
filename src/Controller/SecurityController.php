<?php

namespace App\Controller;
use App\Entity\User;
use App\Form\UserType;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Encoder\passwordEncoder;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="login")
     */
    public function loginAction(Request $request, AuthenticationUtils $authenticationUtils)
    {
        // Si le visiteur est déjà identifié, on le redirige vers l'accueil
//            return $this->redirectToRoute('oc_platform_accueil');
       // }

        // Le service authentication_utils permet de récupérer le nom d'utilisateur
        // et l'erreur dans le cas où le formulaire a déjà été soumis mais était invalide
        // (mauvais mot de passe par exemple)
//$authenticationUtils = $this->get('security.authentication_utils');

       // return $this->render('Login/login.html.twig', array(
     //   'last_username' => $authenticationUtils->getLastUsername(),
      //  'error'         => $authenticationUtils->getLastAuthenticationError(),
     //   ));

        //Récupères les erreurs de connexion s'il y en a
        $error = $authenticationUtils->getLastAuthenticationError();

        // Récupères l'identifiant rentré par l'utilisateur
        $lastUsername = $authenticationUtils->getLastUsername();

        //Renvoie l'utilisateur sur la page d'acceuil si la connexion est échouée.
        return $this->render('Login/login.html.twig', ['last_username' => $lastUsername,'error'=> $error] );
    }

    /**
     * @Route("/createuser", name="createuser")
     */
    public function createUser()
    {
       
    }


}