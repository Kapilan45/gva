<?php

namespace App\Controller;

use App\Entity\Client;
use App\Entity\Billet;
use App\Entity\Vol;
use App\Entity\Escale;
use App\Form\ClientType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;


class PayPalController extends AbstractController
{
    public function paypal($data, $prixA, $prixR = 0)
    {
       
        $payeur = (new \PayPal\Api\Payer())->setPaymentMethod("paypal");
        
       
        $details = (new \PayPal\Api\Details())->setSubTotal(($prixA + $prixR));
        $amount = (new \PayPal\Api\Amount())->setCurrency("EUR")->setTotal(($prixA + $prixR))->setDetails($details);
        //$amount->setDetails($detail);
        $transaction = (new \PayPal\Api\Transaction())->setAmount($amount)->setItemList($list)->setDescription("Achat sur Snir Airlines")->setCustom("kan");

        $redirectUrls = new \PayPal\Api\RedirectUrls();
        $rurl = $this->generateUrl('paycatch',[], UrlGeneratorInterface::ABSOLUTE_URL);
        $redirectUrls->setReturnUrl($rurl.'?'.$data);
        $curl = $this->generateUrl('reservation',[], UrlGeneratorInterface::ABSOLUTE_URL);
        $redirectUrls->setCancelUrl($curl.'?'.$data);

        $payment = new \PayPal\Api\Payment();       
        $payment->setIntent("sale");
        $payment->setPayer($payeur);
   
    }

    /**
     * @Route("/paycatch", name="paycatch")
     */
    public function paycatch(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $em_billet = $this->getDoctrine()->getRepository(Billet::class);
        $em_client = $this->getDoctrine()->getRepository(Client::class);

        $billetId = $_REQUEST["baId"];
        $clientId = $_REQUEST["cId"];

       
        $payment = \PayPal\Api\Payment::get($_GET["paymentId"], $apicontext);

        $execution = new \PayPal\Api\PaymentExecution();
        $execution->setPayerId($_GET["PayerID"]);
        $execution->setTransactions($payment->getTransactions());

        try{
            

        }catch(\PayPal\Exception\PayPalConnectionException $e) {
            dump(json_decode($e->getData()));
            return $this->render('Paypal/paypal.html.twig', ['dd' => json_decode($e->getData())]);
        }        
    }

    // Annulation de paiement Paypal
    public function cancelPay($data, Request $request)
    {
        dump($data);
        $em = $this->getDoctrine()->getManager();
        $em_billet = $this->getDoctrine()->getRepository(Billet::class);
        $em_vol = $this->getDoctrine()->getRepository(Vol::class);
        $em_es = $this->getDoctrine()->getRepository(Escale::class);

        if(isset($data["cId"]))
        {
            $cId = $data["cId"];
            $bId = $data["baId"];
            $brId = $data["brId"];

            $vols = $em_billet->findBy(["client_id"=> $cId]);
            dump($vols);

            $client = $em->getRepository('App:Client')->findBy(['id'=>$cId]);
            $formclient = $this->createForm(ClientType::class,$client[0]);

            foreach ($vols as $cle => $value) {
                if(($value->getEscaleId() == null))
                { $vol[$cle] = $em_vol->find(($vols[$cle]->getVol()->getId())); }
                else
                {
                    $voltmp = $em_es->find($value->getEscale()->getId());
                    $tmpv = json_decode($voltmp->getEscale());
                    $tmpvolA = $em_vol->find($tmpv->e0);
                    $tmpvolA2 = $em_vol->find($tmpv->e1);
                    $vol[$cle] = ["0" => $tmpvolA, "1"=> $tmpvolA2];
                }
            }

            $volA = $vol[0];
            $volR = $vol[1];

            $formclient->handleRequest($request);
            if($formclient->isSubmitted() && $formclient->isValid())
            {
                dump($client);
                $em->persist($client[0]);
                $em->flush();
                dump($volA);
                dump($volR);
                $ids = ("&baId=".$bId."&brId=".$brId."&cId=".$client[0]->getId());      
            }

            if($vols){              
                $view = $this->renderView("Reservation/form_inscription.html.twig", ["volaller" => $volA, "volretour" => $volR, "typesearchvol"=>"allerretour", "recapa" => ["catb" => "CLASSIC"], "recapr" => ["catb" => "CLASSIC"], "reservation" => $formclient->createView() ]);
            }
            return new JsonResponse(['Success' => true, 'code' => 200, "view" => $view]);
        }
        return new Response("<html> <body>La page que vous essaiez d'acc�der n'est pas valide </body> </html>");
    }

}