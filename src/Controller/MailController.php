<?php

namespace App\Controller;

use App\Entity\Billet;
use App\Entity\Client;
use App\Entity\Vol;
use App\Entity\Escale;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use Spipu\Html2Pdf\Html2Pdf;

class MailController extends AbstractController
{
    /**
     * @Route("/testmail", name="testmail")
     */
    public function index(\Swift_Mailer $mailer)
    {
        $em_b = $this->getDoctrine()->getRepository(Billet::class);
        $client = $this->getDoctrine()->getRepository(Client::class)->find(63);
        $billet = $em_b->find(102);
        dump($billet);
        $brId = 45;
        // Génération de pdf
        $volaller = $this->billetPdf($billet);
        if($brId != 0){ 
            $billet1 = $em_b->find(103);
            $volretour = $this->billetPdf($billet1); // Génération de pdf
        }       

        $numbillet = $billet->getId().$billet->getClientId().$billet->getVolId().$billet->getEscaleId().$billet->getPrix();
        
        $billet = $this->renderView("Reservation/billet.html.twig", ["numbillet"=> $numbillet, "volaller"=> $volaller, "client" => $client, "volretour" => $volretour]);

        $html2pdf = new Html2Pdf();
        $html2pdf->writeHTML($billet);
       //$html2pdf->output();
        $content = $html2pdf->Output('', 'S');

        $contenu = $this->renderView("Reservation/contenu_mail.html.twig", ["client" => $client ]);
        // Envoi de mail
        //return $this->envoiMail($client->getMail(), $content, $contenu, $mailer);
       //return $this->forward("App\Controller\MailController::envoiMail", ["smail" => $client->getMail(), "content" => $content, "contenu" => $contenu]);

       return $this->render('Reservation/billet.html.twig',["numbillet"=> $numbillet, "volaller"=> $volaller, "volretour" => $volretour, "client" => $client]);
        /*
        return $this->render('mail/index.html.twig', [
            'controller_name' => 'MailController',
        ]);*/
    }


    // génération de pdf
    public function billetPdf($billet)
    {
        $em_vol = $this->getDoctrine()->getRepository(Vol::class);
        if($billet->getEscaleId() != null){
            $vol = $this->getDoctrine()->getRepository(Escale::class)->find($billet->getEscale()->getId());
            $tmpes = json_decode($vol->getEscale());
            $tmpaes1 = $em_vol->find($tmpes->e0);
            $tmpaes2 = $em_vol->find($tmpes->e1);
            $aes = ["0" => $tmpaes1, "1" => $tmpaes2];
            return $aes;
        }
        else{ return $em_vol->find($billet->getVolId());  /*$vol*/ }               
    }


    // fonction permettant d'envoyer un mail en utilisant bundle SwiftMailer
    public function envoiMail($smail, $content, $contenu, $mailer)
    {
        //$smail = "mahenthirarajahk@gmail.com";
        $attachment = (new \Swift_Attachment($content, 'e-billet.pdf', 'application/pdf'));

        // Créer la communication avec mail et mdp avec setUsername() and setPassword()
        $transport = (new \Swift_SmtpTransport('smtp.gmail.com', 465, 'ssl'))
          ->setUsername('mmzkkp@gmail.com')
          ->setPassword('mister95snir');

        // Créer un mail
        $mailer = new \Swift_Mailer($transport);

        // Les informations des mails
        $message = (new \Swift_Message)
            ->setFrom(array("snirairlines@snir.com" => 'snir Airlines'))
            ->setTo(array($smail => $smail));
        $message->setBody($contenu, 'text/html');
        //$message->setBody('<h3>Bonjour Monsieur</h3>', 'text/html');
        $message->attach($attachment);

        $mailer->send($message); // envoyer mail

        return $this->render("Reservation/reservation_success.html.twig");
    }
}
