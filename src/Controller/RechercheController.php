<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Vol;
use App\Entity\Aeroports;
use App\Entity\Compagnies;

class RechercheController extends AbstractController
{
    /**
     * @Route("/recherche", name="recherche")
     */
    public function recherche(Request $request)
    {
          // On vérifie que l'utilisateur dispose bien du rôle ROLE_AUTEUR
   /* if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
      // Sinon on déclenche une exception « Accès interdit »
      throw new AccessDeniedException('Accès limité aux auteurs.');
    }*/
        $em_airports = $this->getDoctrine()->getRepository(Aeroports::class);
        $em_compagnies = $this->getDoctrine()->getRepository(Compagnies::class);
        $data = $request->request->all();
        if($request->getMethod() == "POST" && $request->isXmlHttpRequest())
        {
            if($data['dom'] == "compagnies")
            {
                $msg = $em_compagnies->findByCompagnies($data['val']);
            }
            else
            {
                $msg = $em_airports->findByVille($data['val']);
            }
            
            $view = $this->renderView("Recherche/affiche_ville.html.twig", ["aeroports" => $msg]);
            dump($msg);
            return new JsonResponse(["view" => $view]);
        }
    	return $this->render('Recherche/accueil.html.twig');
    }




    /**
     * @Route("/recherche/view", name="recherche/view")
     */
    public function rechercheView(Request $request)
    {
    	$em = $this->getDoctrine()->getRepository(Vol::class);
        $data = $request->request->all();
    		
    	if(($request->getMethod() == "POST") && ($request->isXmlHttpRequest()))
    	{
    		// recherche des vol pour le cas de retour + recaptilatutif de vol aller choisi
    		if(isset($_REQUEST["volaller"]))
    		{
                dump($data);
    			// Récapituatif de vol aller choisi pour 3 cas ("vol direct", "avec un escale", "avec 2 escale")
    			$volaller = $em->find($data["volaller"]);
    			if(isset($_REQUEST["es1"]))
    			{
    				$volaller2 = $em->find($data["es1"]);    			 	
    			 	if(isset($_REQUEST["es2"]))
    			 	{    			 		
	    			 	$volaller3 = $em->find($data["es2"]);
	    			 	$volaller = array('0' => $volaller, '1' => $volaller2, '2' => $volaller3);
    			 	}
    			 	else{ $volaller = array('0' => $volaller, '1' => $volaller2); }    			 	
    			}

    			// recherche des  vols disponible le cas de retour	
                if(isset($data['filtre']))
                    { $vols = $em->findByVolDirect($data['da'], $data['va'], $data['vd'], $data['np']); }
                else
                {
                    $arrivee_esc = date("Y-m-d",strtotime($data['da']."+ 2 days"));
                    $resultat = $em->findByEscaleVol($data['da'], $arrivee_esc, $data['np']);
                    $vols = $this->filtreEscale($resultat, $data['va'], $data['vd']); 
                }			
				

				// return vols dispo ou pas   		
    			if($vols){
    				$view = $this->renderView('Recherche/vols_aller_retour.html.twig', ["vols"=>$vols, "volaller"=>$volaller, "recap"=>$data]);
    			}
    			else{ $view = $this->renderView('Recherche/aucun_vol.html.twig'); }
    			
				return new JsonResponse(array('Success' => true, 'code' => 200, "view" => $view));				
    		}

            dump($data);
    		// Recherche des vols aller disponibles
            if(isset($data['filtre'])){ $vols = $em->findByVolDirect($data['dd'], $data['vd'], $data['va'], $data['np']); }
            else
            {
                $depart_esc = date("Y-m-d",strtotime($data['dd']."+ 2 days"));
                $resultat = $em->findByEscaleVol($data['dd'], $depart_esc, $data['np']);
                $vols = $this->filtreEscale($resultat, $data['vd'], $data['va']);  
            }
    		

    		if($vols){
    			if($data['da'] == "2010-01-01")
    				$view = $this->renderView('Recherche/vols_aller_retour.html.twig', ["vols"=>$vols, "recap"=>$data]);
    			else
    				$view = $this->renderView('Recherche/vols_aller_retour.html.twig', ["vols"=>$vols, "recap"=>$data]);
    		}	    			
    		else{ $view = $this->renderView('Recherche/aucun_vol.html.twig'); }

    		return new JsonResponse(array('Success' => true, 'code' => 200, "view" => $view));
    	}
        return $this->render('Recherche/recherche.html.twig');
    }


    }

}
