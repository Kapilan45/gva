<?php

namespace App\Controller;

use App\Entity\Vol;
use App\Form\VolType;
use App\Entity\Client;
use App\Form\ClientType;
use App\Entity\Billet;
use App\Entity\Escale;
use App\Form\SearchType;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Session;

use Spipu\Html2Pdf\Html2Pdf;


class ReservationController extends AbstractController
{

    /**
     * @Route("/reservation", name="reservation")
     */
    public function reservation(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $em_vol = $this->getDoctrine()->getRepository(Vol::class);
        $data = $request->request->all();
        

        $client = new Client();
        $billet = new Billet();
        $billetR = new Billet();
        $escale = new Escale();
        $escaleR = new Escale();
        $form = $this->createForm(ClientType::class,$client);

        // Récupérer les informations des clients et les vols choisi par celle-ci
        if(($request->getMethod() == "POST") && ($request->isXmlHttpRequest()))
        {
            if(isset($data['cId']))
            { return $this->forward("App\Controller\PayPalController::cancelpay", ["data" => $data]);  }
            if(isset($data["vold"]))
            {
                // Affichage de récaptulatif des vols aller et retour choisis
                $volA = $em_vol->find($data["vold"]);
                $volR = $em_vol->find(intval($data["volr"]));

                // Gestion des escales de vol aller
                if(isset($data["aes1"]))
                {
                    $volE1 = $em_vol->find($data["aes1"]);
                    $volaller = array('0' => $volA, '1' => $volE1 );
                    if(isset($data["aes2"]))
                    {
                        $volE2 = $em_vol->find($data["aes2"]);
                        $this->createEscale($escale, $volA, $volE2, $volE1);
                        $volaller[2] = $volE2;
                    }
                    else{ $this->createEscale($escale, $volA, $volE1); }
                }
                else{ $volaller = $volA; }

                // Gestion des escales de vol retour
                if($volR)
                {
                    if(isset($data["es1"]))
                    {
                        $volRE1 = $em_vol->find($data["es1"]);
                        $volretour = array('0' => $volR, '1' => $volRE1 );
                        if(isset($data["es2"]))
                        {
                            $volRE2 = $em_vol->find($data["es2"]);
                            $this->createEscale($escaleR, $volR, $volRE2, $volRE1);
                            $volretour[2] = $volRE2;
                        }
                        else { $this->createEscale($escaleR, $volR, $volRE1); }
                    }
                    else{ $volretour = $volR; }
                }

                // start formulaire
                // Formule d'inscription validé mise à jour bdd (finalisation)
                $form->handleRequest($request);
                if($form->isSubmitted() && $form->isValid())
                {
                    // Enregister les informations des clients dans bdd
                    $em->persist($client);
                    $em->flush();

                    // Le cas où le client a choisi vol aller
                    if($volA)
                    {
                        // le cas du vol escale + insertion dans bdd avec relation client
                        if(isset($data["aes1"])) // ok funct
                        {
                            $prixEscale = ($volA->getPrix() + $volE1->getPrix());
                            if(isset($data["aes2"]))
                            { $prixEscale = ($volA->getPrix() + $volE1->getPrix() + $volE2->getPrix()); }
                            $this->createBillet($em, $billet, $volA, $client, $escale, $prixEscale);
                            $ids = ("&baId=".$billet->getId()."&cId=".$client->getId());
                        }
                        // le cas de vol direct
                        else
                        {
                            $this->createBillet($em, $billet, $volA, $client);
                            $ids = ("&baId=".$billet->getId()."&cId=".$client->getId());
                        }
                    }

                    // Le cas où le client a choisi vol retour
                    if($volR)
                    {
                        // le cas du vol escale + insertion dans bdd avec relation client
                        if(isset($data["es1"])) // ok funct
                        {
                            $prixEscale = ($volR->getPrix() + $volRE1->getPrix());
                            if(isset($data["es2"]))
                            { $prixEscale = ($volR->getPrix() + $volRE1->getPrix() + $volRE2->getPrix()); }

                            $this->createBillet($em, $billetR, $volR, $client, $escaleR, $prixEscale);
                            $ids = ("&baId=".$billet->getId()."&brId=".$billetR->getId()."&cId=".$client->getId());
                        }
                        // le cas de vol direct
                        else
                        {
                            $this->createBillet($em, $billetR, $volR, $client);
                            $ids = ("&baId=".$billet->getId()."&brId=".$billetR->getId()."&cId=".$client->getId());
                        }
                    }

                    // paiement redirection vers paypal
                    return $this->forward("App\Controller\PayPalController::paypal", ["data" => $ids, "prixA" => $billet->getVol()->getPrix(), "prixR" => $billetR->getVol()->getPrix()]);
                }
            }
dump($data);
            if($volR)
                $view = $this->renderView('Reservation/form_inscription.html.twig', ["volaller" => $volaller, "volretour" => $volretour, "typesearchvol" => "allerretour", "recapa" => ["catb" => $data['catba']], "recapr" =>["catb" => $data["catbr"]], "reservation" => $form->createView()]);
            else
                $view = $this->renderView('Reservation/form_inscription.html.twig', ["volaller" => $volaller, "typesearchvol" => "allersimple", "recapa" => ["catb" => $data['catbr']], "reservation" => $form->createView()]);

                return new JsonResponse(array('Success' => true, 'code' => 200, "view" => $view));
        }
        return $this->render("Reservation/reservation.html.twig");
    }



    // Finalisation la réservation de billet avec envoi de billet éléectronique en mail
    public function finalReservation($cId, $bId, $brId = 0, \Swift_Mailer $mailer)
    {
        $em_b = $this->getDoctrine()->getRepository(Billet::class);
        $client = $this->getDoctrine()->getRepository(Client::class)->find($cId);
        $billet = $em_b->find($bId);
        dump($billet);
        
        $volaller = $this->billetPdf($billet); // Recupération des détails de vol aller pour pdf
        if($brId != 0){ 
            $billet1 = $em_b->find($brId);
            $volretour = $this->billetPdf($billet1); // Récupération des détails de vol retour pour pdf
        }       

        $numbillet = $billet->getId().$billet->getClientId().$billet->getVolId().$billet->getEscaleId().$billet->getPrix();
        $billet->setBillet($numbillet);
        
        $billetpdf = $this->renderView("Mail/billet.html.twig", ["numbillet"=> $numbillet, "volaller"=> $volaller, "client" => $client, "volretour" => $volretour]);

        // Génération de pdf
        $html2pdf = new Html2Pdf();
        $html2pdf->writeHTML($billetpdf);
        //$html2pdf->output();
        $content = $html2pdf->Output('', 'S');

        $contenu = $this->renderView("Mail/contenu_mail.html.twig", ["client" => $client ]);
        // Envoi de mail
        return $this->forward("App\Controller\MailController::envoiMail", ["smail" => $client->getMail(), "content" => $content, "contenu" => $contenu, "mailer" => $mailer]);
       

        //return $this->render('Reservation/billet.html.twig',["numbillet"=> $numbillet, "volaller"=> $volaller, "volretour" => $volretour, "client" => $client]);
    }

    // Récupération des détails vols pour générer e-billet (pdf)
    public function billetPdf($billet)
    {
        $em_vol = $this->getDoctrine()->getRepository(Vol::class);
        if($billet->getEscaleId() != null){
            $vol = $this->getDoctrine()->getRepository(Escale::class)->find($billet->getEscale()->getId());
            $tmpes = json_decode($vol->getEscale());
            $tmpaes1 = $em_vol->find($tmpes->e0);
            $tmpaes2 = $em_vol->find($tmpes->e1);
            $aes = ["0" => $tmpaes1, "1" => $tmpaes2];
            return $aes;
        }
        else{ return $em_vol->find($billet->getVolId());  /*$vol*/ }               
    }

    // creation de billet
    public function createBillet($em, $billet, $vol, $client, $escale = null, $prix = null)
    {
    	$billet->setNumBillet(mt_rand(99, 1000));
		$billet->setVol($vol);
		$billet->setClient($client);
		$billet->setPrix($prix);
		$billet->setEscale($escale);
		$em->persist($billet);
		$em->flush();
    }

    // cerateEscale = $escale entity, $premiervol, $derniervol, $volintermediaire ou pas
    public function createEscale($escale, $esfirst, $eslast, $esmiddle = null)
    {
    	$em = $this->getDoctrine()->getManager();
    	//$escale = new Escale();
    	if($esmiddle)
			$escale->setEscale('{"e0":'.$esfirst->getId().', "e1": '.$esmiddle->getId().', "e2":'.$eslast->getId().'}');
    	else
    		$escale->setEscale('{"e0":'.$esfirst->getId().', "e1":'.$eslast->getId().'}');

		$escale->setDepart($esfirst->getDepart());
		$escale->setHeureDepart($esfirst->getHeureDepart());
		$escale->setArrivee($eslast->getArrivee());
		$escale->setHeureArrivee($eslast->getHeureArrivee());
		
		$em->persist($escale);
		$em->flush();
    }

}