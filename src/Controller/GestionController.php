<?php

namespace App\Controller;

use App\Entity\Client;
use App\Entity\Vol;
use App\Entity\Billet;
use App\Entity\Escale;
use App\Entity\Aeroports;
use App\Entity\Compagnies;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;


class GestionController extends AbstractController
{

    /**
     * @Route("/vvol", name="vvol")
     */
    public function vvol(Request $request)
    {
        $h_a = date("Y-m-d H:i:s");  // heure actuelle
        $h_ac = date("Y-m-d");
        $vol = false;

        $v = $this->getDoctrine()->getRepository(Vol::class);
        $billet = $this->getDoctrine()->getRepository(Billet::class)->find(["num_billet" => 456]);
        
        foreach ($billet as $key => $value) 
            if(!$vol)
                $vol = $v->findByCarte($h_ac, $h_a, $value->getVolId());

        return $this->render('Recherche/accueil.html.twig');
    }



    /**
     * @Route("/ajoutville", name="ajouville")
     */
    public function ajoutville(Request $request)
    {
        $monfichier = fopen('aeroportsFinal.txt', 'r');
        $ligne = fgets($monfichier);
        $temp = explode(',', $ligne);
        
        $sql = "('".$temp[4]."','1','".$temp[6]."','".$temp[7]."','".$temp[3]."')";
        for($i=1; $i<32; $i++){
            $ligne = fgets($monfichier);
            $temp = explode(',', $ligne);
           $sql = $sql.",('".$temp[4]."','".($i+1)."','".$temp[6]."','".$temp[7]."','".$temp[3]."')";
        }
        fclose($monfichier);
        return $this->render('Recherche/accueil.html.twig');
    }

    /**
     * @Route("/ajoutcompagnies", name="ajoutcompagnies")
     */
    public function ajoutcompagnies(Request $request)
    {
        $monfichier = fopen('compagniesfinal.txt', 'r');
        $ligne = fgets($monfichier);
        $temp = explode(',', $ligne);
        $sql = "('".$temp[2]."','1','".$temp[0]."')";
        for($i=1; $i<20; $i++){
            $ligne = fgets($monfichier);
            $temp = explode(',', $ligne);
            $sql = $sql.",('".$temp[2]."','".($i+1)."','".$temp[0]."')";            
        }
        fclose($monfichier);
        dump($sql);
        return $this->render('Recherche/accueil.html.twig');
    }

    /**
     * @Route("/ajoutbddvol", name="ajoutbddvol")
     */
    public function ajoutbddvol(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $monfichier = fopen('vols.txt', 'r');

        //$ligne = fgets($monfichier);
        //$temp = explode(',', $ligne);
        /* $vol = new Vol();
            $vol->setPrix(250);
            $vol->setPlaces(50);
            $vol->setNumeroVol($temp[1]);
            $comp = $em->getRepository(Compagnies::class)->findBy(["nom" => $temp[2]]);
            $vol->setVolCompagnies($comp[0]);
            $air = $em->getRepository(Aeroports::class);
            $vol->setArriveeAeroports($air->findBy(["ville" => $temp[5]])[0]);
            $vol->setDepartAeroports($air->findBy(["ville" => $temp[4]])[0]);
            $vol->setHeureDepart(new \DateTime($temp[6]));
            $vol->setHeureArrivee(new \DateTime($temp[7]));
            $vol->setDuree(new \DateTime($temp[8]));
            dump($vol);
            //dump($temp);
            $em->persist($vol);
            $em->flush();*/
 
        for($i=0; $i<7; $i++){
            $ligne = fgets($monfichier);
            $temp = explode(',', $ligne);

            $vol = new Vol();
            $vol->setPrix(intval($temp[9]));
            $vol->setPlaces(50);
            $vol->setNumeroVol($temp[1]);
            $comp = $em->getRepository(Compagnies::class)->findBy(["nom" => $temp[2]]);
            $vol->setVolCompagnies($comp[0]);
            $air = $em->getRepository(Aeroports::class);
            $vol->setArriveeAeroports($air->findBy(["ville" => $temp[5]])[0]);
            $vol->setDepartAeroports($air->findBy(["ville" => $temp[4]])[0]);
            $vol->setHeureDepart(new \DateTime($temp[6]));
            $vol->setHeureArrivee(new \DateTime($temp[7]));
            $vol->setDuree(new \DateTime($temp[8]));
            dump($vol);
            //dump($temp);
            $em->persist($vol);
            $em->flush();
        }
        fclose($monfichier);

        return $this->render('Recherche/accueil.html.twig');
    }




    /**
     * @Route("/gestionvol", name="gestionvol")
     */
    public function gestionVol(Request $request)
    {
    	$em_billet = $this->getDoctrine()->getRepository(Billet::class);
    	$em_es = $this->getDoctrine()->getRepository(Escale::class);
    	$em_vol = $this->getDoctrine()->getRepository(Vol::class);
    	$em_client = $this->getDoctrine()->getRepository(Client::class);
        dump($request);
        if(isset($_REQUEST['bn']))
        {
            dump("hh");
        }

        if($request->getMethod() == "POST" && $request->isXmlHttpRequest())
        {
            $data = $request->request->all();
            dump($data);
            if(isset($data["bn"]))
            {
                dump("hhh");
                $tmpbillet = $em_billet->findBy(["num_billet" => $data['bn']]);
                $client = $em_client->find($tmpbillet[0]->getClientId());
                dump($client);

                $billets = $em_billet->findBy(["client_id"=> $client->getId()]);
                dump($billets);

                foreach ($billets as $cle => $value) {
                    if(($value->getEscaleId() == null)){
                        $vol[$cle] = $em_vol->find(($billets[$cle]->getVol()->getId()));    
                    }
                    else{
                        $voltmp = $em_es->find($value->getEscale()->getId()); // récupère les escales
                        $tmpvols = json_decode($voltmp->getEscale());

                        $tmpvolA = $em_vol->find($tmpvols->e0);
                        $tmpvolA2 = $em_vol->find($tmpvols->e1);
                        //$tmpvolA = $em_vol->find($voltmp->getEscale()["e0"]);
                        //$tmpvolA2 = $em_vol->find($voltmp->getEscale()["e1"]);
                        $vol[$cle] = ["0" => $tmpvolA, "1"=> $tmpvolA2];
                    }
                }
                $volA = $vol[0];
                $volR = $vol[1];

                if(isset($data['gesvolmod']))
                {
                    $view = $this->renderView("Paypal/paypal.html.twig");
                    return new JsonResponse(["Success"=> true, "view" => $view]);
                }

                $view = $this->renderView("Gestion/affiche.html.twig",  ["client"=>$client, "volaller" => $volA, "volretour" => $volR, "typesearchvol" => "allerretour", "nbillet"=>$data['bn']]);
                return new JsonResponse(["Success"=> true, "view" => $view]);
            }
        }
    	//$nbillet = $_REQUEST["billet"];
    	/*if(isset($_REQUEST["billet"]))
    	{
    		$tmpbillet = $em_billet->findBy(["num_billet" => 276]);
    		$client = $em_client->find($tmpbillet[0]->getClientId());
    		dump($client);

    		$billets = $em_billet->findBy(["client_id"=> $client->getId()]);
			dump($billets);

			foreach ($billets as $cle => $value) {
				if(($value->getEscaleId() == null)){
					$vol[$cle] = $em_vol->find(($billets[$cle]->getVol()->getId()));	
				}
				else{
					$voltmp = $em_es->find($value->getEscale()->getId());
					$tmpvolA = $em_vol->find($voltmp->getEscale()["e0"]);
	    			$tmpvolA2 = $em_vol->find($voltmp->getEscale()["e1"]);
	    			$vol[$cle] = ["0" => $tmpvolA, "1"=> $tmpvolA2];
				}
			}
			$volA = $vol[0];
			$volR = $vol[1];

            if(isset($_REQUEST["gesvolmod"])){
                $view = $this->renderView("Paypal/paypal.html.twig");
                return new JsonResponse(["Success"=> true, "view" => $view]);
            }

    		$view = $this->renderView("Gestion/affiche.html.twig",  ["client"=>$client, "volaller" => $volA, "volretour" => $volR, "typesearchvol" => "allerretour", "nbillet" => 276]);
    		return new JsonResponse(["Success"=> true, "view" => $view]);
    	}*/

    	return $this->render('Gestion/gestion_vol.html.twig');
    }


    /**
     * @Route("/ajoutvol", name="ajoutvol")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function ajoutvol(Request $request)
    {
    	$em = $this->getDoctrine()->getManager();    	
    	$em_aeroports = $this->getDoctrine()->getRepository(Aeroports::class);

    	$vol = new Vol();
    	$form = $this->createForm(VolType::class,$vol);
    	
    	if(($request->getMethod() == "POST") && ($request->isXmlHttpRequest()))
    	{
    		$form->handleRequest($request);
    		if($form->isSubmitted() && $form->isValid())
    		{
    			$depart = $em_aeroports->findBy(['ville'=> ($vol->getDepart())]);
    			$arrivee = $em_aeroports->findBy(['ville'=> ($vol->getArrivee())]);
				$compagnies = $this->getDoctrine()->getRepository(Compagnies::class)->findBy(['nom'=> ($vol->getCompagnies())]);

				if($depart && $arrivee && $compagnies)
				{
					$vol->setDepartAeroports($depart[0]);
	    			$vol->setArriveeAeroports($arrivee[0]);
	    			$vol->setVolCompagnies($compagnies[0]);
	    			//dump($vol);

	    			$em->persist($vol);
	    			$em->flush();
	    			return new JsonResponse(array('Success' => true, 'code' => 200, 'view'=>'succes'));
				}
				else
				{
					//$view = $this->renderView('Reservation/dispovol.html.twig');
            		return new JsonResponse(array('Success' => true, 'code' => 200, 'view'=>'errer'));
				}	
    		}
    	}
		return $this->render("Gestion/ajoutvol.html.twig",["ajoutvol" => $form->createView() ]);
    }
    
}
