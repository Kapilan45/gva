AF,1001,Air France,MAD,Madrid,12:05 PM,Atterri
DL,8541,Delta Air Lines,MAD,Madrid,12:05 PM,Atterri
U2,3994,easyJet,NCE,Nice,2:45 PM,prevu retard
IB,4940,Iberia,DFW,Dallas,12:45 PM,Atterri
GF,6626,Gulf Air,DFW,Dallas,12:45 PM,Atterri
AY,6308,Finnair,NCE,Nice,12:10 PM,Atterri
VY,8830,Vueling,SVQ,Sevilla,12:50 PM,Atterri
MU,8667,China Eastern Airlines,PEK,Beijing,2:05 PM,En Route
CZ,7001,China Southern Airlines,FCO,Rome,12:10 PM,Atterri
BA,8088,British Airways,LGW,London,1:50 PM
UL,3539,SriLankan Airlines,DOH,Doha,1:40 PM,Atterri
AR,7836,Aerolineas Argentinas,MAD,Madrid,12:05 PM,Atterri
UA,6926,United Airlines,ISL,Istanbul,12:45 PM,Atterri
VS,6776,Virgin Atlantic,JFK,New York,1:10 PM,Atterri
EY,2896,Etihad Airways,FCO,Rome,12:30 PM,Atterri
QR,39,Qatar Airways,DOH,Doha,1:40 PM,Atterri
MK,9653,Air Mauritius,OSL,Oslo,2:50 PM,En Route
JU,7802,Air Serbia,DUB,Dublin,2:25 PM,En Route
LX,4608,SWISS,GVA,Geneva,2:15 PM,En route 
BE,4101,Flybe,MAD,Madrid,12:05 PM
AA,22,American Airlines,DFW,Dallas,12:45 PM,Atterri
UX,2227,Air Europa,MAD,Madrid,12:05 PM,Atterri
G3,8535,Gol,NUE,Nuremberg,12:05 PM,Atterri
UU,8775,Air Austral,OSL,Oslo,2:50 PM,En Route
VN,3683,Vietnam Airlines,BCN,Barcelona,12:05 PM,
KL,2380,KLM,LAX,Los Angeles,2:20 PM,En
SU,3150,Aeroflot,CFE,Clermont-Ferrand,12:10 PM,
KE,6380,Korean Air,MAD,Madrid,12:05 PM,Atterri
AZ,2549,Alitalia,CFE,Clermont-Ferrand,12:10 PM
AF,7741,Air France,TLN,Toulon,2:55 PM,Prévu
DL,8541,Delta Air Lines,MAD,Madrid,12:05 PM,Atterri
KK,7467,Atlasglobal,MAD,Madrid,12:05 PM,Atterri
CI,9377,China Airlines,AMS,Amsterdam,1:40 PM,En route retard
MH,4599,Malaysia Airlines,DXB,Dubai,1:30 PM,Atterri
EK,73,Emirates,DXB,Dubai,1:30 PM,Atterri
SQ,1891,Singapore Airlines,TLS,Toulouse,2:40 PM,En Route
AH,1000,Air Algerie,ALG,Algiers,2:00 PM,En route retard
A3,3384,Aegean Airlines,OSL,Oslo,1:25 PM,Atterri
CA,834,Air China,PVG,Shanghai,12:15 PM,En Route
KQ,3006,Kenya Airways,LYS,Lyon,12:20 PM,Atterri
AC,6692,Air Canada,ISL,Istanbul,2:15 PM,Prévu
3O,364,Air Arabia Maroc,TNG,Tangier,2:40 PM,Prévu