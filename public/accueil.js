// Recherche et affichage des nom des aéroports pour recherche et ajout de vol en 2 fonctions
/*$(document).on("keypress", "input[type=text]", function(){
    var valeur = $(this).val();
    var name = $(this).attr("id");
    var domaine = $(this).attr("data");

    if(valeur.length > 0)
    {
        var request = $.ajax({
            url : "accueil",
            method: "POST",
            data: "&val=" + valeur + "&dom=" + domaine
        });
        request.done(function(msg){
            $("#liste_" + name).html(msg.view);
        });
    }
});

$(document).on("click", ".list-group-item", function () {
    $(this).parent().parent().children("input").val($(this).html());
    $(this).parent().html("");
});*/


// Gestion des vols pour user
$(document).on("submit", "form[name=gestionvol]", function(e){
    e.preventDefault();
    var data = $(this).serialize();
    window.location.href= "gestionvol?&" + data;
});



// choix de recherche de vol "aller seul" ou "aller et retour"
$(document).on("change", "input[type=radio]", function(){
    var valradio = $(this).attr("value");
    if(valradio == "allersimple")
        $("#radiosimple").addClass("d-none").children("input").attr("value", "2010-01-01");
    else
        $("#radiosimple").removeClass("d-none").children("input").attr("value", "");
});

// Formule de recherche de vol submit fonction
$(document).on("submit", "form[name=recherche]", function(e){
    e.preventDefault();
    var data = $(this).serialize();
    window.location.href= "recherchevol?&" + data;
    //window.location.href= "{{path('recherchevol')}}?&" + data;
});



// Sélectionne de vol aller 
$(document).on("click", ".vol", function(){  
    $("#loadmodal").click();

    var volid = $(this).attr("name"); // id de vol
    var tvol = $(this).data("type"); // type de vol direct ou escale
    var data = location.search;
    data = data.replace("allerretour", "last");
    var typevol = $(".typevol").data("typevol");
    
    // vol aller et retour
    if(typevol=="allerretour")
    {
        var request = $.ajax({
            url: "recherchevol",
            method: "POST",
            data: data + "&volaller=" + volid + "&typevol=" + tvol
        });
        request.done(function(msg){
            $(".container").html(msg.view);
            $("#loadmodal").click();
        });     
    }
    else
    {
        var voldid = $(".detailVolAller").attr("id");
        var chdefault = /escale/gi;
        var chd = /es/gi;
        if(voldid){
            if((voldid.length) > 2){ voldid = voldid.replace(chd, "aes"); }
            window.location.href= "reservation?&vold=" + voldid + "&volr=" + volid + "&typevol=" + tvol;
        }           
        else{
            if((volid.length) > 2){ volid = volid.replace(chdefault, "aescale");  }
            window.location.href= "reservation?&vold=" + volid + "&volr=" + voldid + "&typevol=" + tvol;        
        }
    } 
});

// Modifier le vol Aller choisi
$(document).on("click", "#changervolaller", function(){  window.location.reload(); });

// Ajoutvol fomulaire submit fonction
$(document).on("submit", "form[name=ajoutvol]", function(e){
    e.preventDefault();
    var data = $(this).serialize();
    var request = $.ajax({
        url: "ajoutvol",
        method: "POST",
        data: data
    });
    request.done(function(msg){
        if(msg.success){ alert(msg.view);  }
    });
});


// Formulaire d'inscription des clients submit fonction
$(document).on("submit", "form[name=client]", function(e){
    e.preventDefault();
    $("#loadmodal").click();
    var data = $(this).serialize();
    var url = location.search;
    var choixpaiement = $(".choixpaiement").attr("name");
   
    var request = $.ajax({
        url: "reservation",
        method: "POST",
        data: data + "&" + url + "&choixpaiement=" + choixpaiement
    });
    request.done(function(msg){
        window.location.href=msg.view;
        //$(".container").html(msg.view);
    });
});



